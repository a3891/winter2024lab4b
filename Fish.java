public class Fish{
	
	private String species;
	private int lengthInInches;
	private boolean isSaltWater;
	
	public Fish (String s, int l, boolean isSalt){
		
		species=s;
		lengthInInches=l;
		isSaltWater=isSalt;
	}
	
	public String nameSpecies(){
		return "This is a " + species + "!";
		
	}
	
	public boolean canSwimInSalt(){
		
		if("Goldfish".equals(species)){
			return false;
			
		}else{
			return true;
		}
	}
	
	//lab 4B
	//modifications by Artem Brandt
	//getters and setters
	public String getSpecies() {
		return this.species;
	}
	public int getlengthInInches() {
		return this.lengthInInches;
	}
	public boolean getisSaltWater() {
		return this.isSaltWater;
	}
	public void setSpecies(String newSpecies) {
		this.species = newSpecies;
	}
	//public void setlengthInInches(int newlengthInInches) {
	//	this.lengthInInches = newlengthInInches;
	//}
	//public void setisSaltWater(boolean newisSaltWater) {
	//	this.isSaltWater = newisSaltWater;
	//}
}